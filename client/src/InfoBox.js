import React from "react";
import "./InfoBox.css";
import { Card, CardContent, Typography } from "@material-ui/core";

function InfoBox({ title, cases, total }) {
  return (
    <Card className="infobox">
      <CardContent>
        <Typography color="textSecondary" className="infobox-title">
          {title}
        </Typography>
        <h2 className="infobox-cases">{cases}</h2>
        <Typography color="textSecondary" className="infobox-total">
          {total} Total
        </Typography>
      </CardContent>
    </Card>
  );
}

export default InfoBox;
